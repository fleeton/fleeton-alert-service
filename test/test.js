var a = require('../lib/index.js')
var assert = require('assert')
var b = {
  'imei': '356917050291991',
  'n_rtime': new Date(),
  'n_speed': 0,
  'n_cog': 0,
  'n_alt': 944,
  'n_nos': 13,
  'n_gps': 1,
  'n_gss': 5,
  'n_dis': 0,
  'n_dos': 0,
  'n_trip': 0,
  'n_psn': 88,
  'n_mv': 0,
  'n_loc': [
    77.58908,
    12.990582
  ],
  'n_aiv': 0,
  'n_eb': 10.88,
  'n_ib': 6.31,
  'n_ptem': 29.55,
  'n_owd': 0,
  'n_agv': 0.99,
  'n_tv': 66,
  'n_chksm': 95,
  'n_vo': 0
}

var c = {
  'imei': '356917050291991',
  'n_dis': '000',
  'n_dos': '00',
  'a_ig': '000000000',
  'rfid': '0',
  'n_rtime': new Date(),
  'n_speed': 0,
  'n_cog': 0,
  'n_alt': 956,
  'n_nos': 13,
  'n_gps': 1,
  'n_gss': 5,
  'n_mv': 0,
  'n_tv': 66,
  'n_trip': 0,
  'n_vo': 0,
  'a_os': 0,
  'a_i2m': 0,
  'a_im': 0,
  'a_etemp': 0,
  'a_ed': 0,
  'a_dr': 0,
  'a_ibl': 0,
  'a_epsr': 0,
  'a_gmf': 0,
  'a_tw': 0,
  'a_snr': 1,
  'a_smode': 1,
  'a_sc': 0,
  'n_psn': 92,
  'n_chksm': 69,
  'n_loc': [
    77.58908,
    12.990582
  ],
  'n_aiv': 0,
  'n_eb': 10.88,
  'n_ib': 6.31,
  'n_ptem': 29.64,
  'n_owd': 0,
  'n_agv': 0.99
}

a.processData(c, function (re) {
  assert.equal(re, true)
  process.exit()
})

// a.processData(b, function (re) {
//   // assert.equal(re, true)
// })
