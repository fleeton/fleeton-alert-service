var fs = require('fs')
var winston = require('winston')
var mongoose = require('mongoose')
var models = require('./models.js')

var __alertOverSpeed = 0
var __alertGeoFence = 1
var __alertIgnition = 2
var __alertpPowercut = 3
var __alertHarshDrive = 4

var cFile = __dirname + '/config.json'
var _config_w = JSON.parse(
  fs.readFileSync(cFile)
)

var _config_k = _config_w.kitana
var _config_m = _config_w.millena

var _auth_string_k, _auth_string_m
if (_config_k.user && _config_k.password)
  _auth_string_k = _config_k.user + ':' + _config_k.password + '@'
else
  _auth_string_k = ''
var cUrl_k = 'mongodb://' + _auth_string_k + _config_k.host + ':' + _config_k.port + '/' + _config_k.database
var kitana = mongoose.createConnection(cUrl_k, { server: { poolSize: 4 }})

if (_config_m.user && _config_m.password)
  _auth_string_m = _config_m.user + ':' + _config_m.password + '@'
else
  _auth_string_m = ''
var cUrl_m = 'mongodb://' + _auth_string_m + _config_m.host + ':' + _config_m.port + '/' + _config_m.database
var millena = mongoose.createConnection(cUrl_m, { server: { poolSize: 4 }})

var Alert = kitana.model('Alert', new mongoose.Schema(models.Alert), 'alert')
var Tracker = kitana.model('Tracker', new mongoose.Schema(models.Tracker), 'tracker')
var Notification = kitana.model('Notification', new mongoose.Schema(models.Notification), 'notification')
var Vehicle = kitana.model('Vehicle', new mongoose.Schema(models.Vehicle), 'vehicle')

winston.add(winston.transports.File, { filename: 'somefile.log' })

exports.processData = function (data, cb) {
  Tracker.findOne({imei: data.imei}, 'vehicleId', function (err, vehicle) {
    if (err) {
      winston.log('error', err)
      return cb(err, null)
    }

    if (!vehicle) {
      winston.log('info', 'Unknown tracker ' + data.imei + ' sent a request!')
      return cb({errorMessage: 'unknown tracker'}, null)
    }
    vehicle = vehicle.toObject()
    var vehicleId = vehicle.vehicleId

    var _vSchema = new mongoose.Schema(models.VehicleData, { timestamps: { createdAt: 'createdAt' } })
    _vSchema.pre('save', function (next) {
      this.rtime = new Date(this.rtime)
      next()
    })
    var _vehicleData = millena.model('Vehicle_' + vehicleId, _vSchema, 'vehicle_' + vehicleId)
    var dSave = new _vehicleData(data)
    dSave.save(function (err) {
      if (err) {
        winston.log('error', err)
        return cb(false)
      } else {
        return cb(true)
      }
    })

  })
}

var check_alert = function (vehicleId, data, cb) {
  Alert.find({vehicleId: vehicleId, isActive: true}, function (err, alert) {
    if (err) {
      winston.log('err', err)
      return cb(err, null)
    }

    if (!alert) {
      return cb(null, true)
    } else {
      for (var i = 0; i < alert.length; i++) {
        (function (_alert, fields) {
          _alertPerDevice(_alert, fields, function (err, notice) {
            if (err) {
              winston.log('err', err)
            }

          // else true
          })
        })(alert[i], data)
      }
    }
  })
}

/*
 * The TLN packets are Normal packets! Thus only overspeed and geofencing should be checked here!
 * The TLA and TLL packets are Alert packets! The overspeed and geofencing are software dependent and are being calculated with TLN and TLB packets! Thus remaining every other alert will be checked with TLA, TLL packets!
 */
var _alertPerDevice = function (alert, data) {
  var actions = []
  for (var i = 0; i < alert.action.length; i++) {
    if (alert.action[i] == '1') {
      actions.push(i)
    }
  }

  for (var i = 0; i < actions.length; i++) {
    switch (actions[i]) {
      case __alertOverSpeed:
        if (data._type == 'TLN' || data._type == 'TLB') {
          (function (id, userId, vehicleId, maxSpeed, maxSpeedAchieved, alertBy, fields) {
            calculateOverSpeed(id, userId, vehicleId, maxSpeed, maxSpeedAchieved, alertBy, fields)
          })(alert.id, alert.userId, alert.vehicleId, alert.maxSpeed, alert.maxSpeedAchieved, alert.alert, fields)
        } else {
          break
        }
        break
      case __alertGeoFence:
        if (data._type == 'TLN' || data._type == 'TLB') {
          // geofence checking function ()
        } else {
          break
        }
        break
      case __alertIgnition:
        if (data._type == 'TLN' || data._type == 'TLB') {
          break
        } else {
          // Ignition checking function ()
        }
        break
      case __alertpPowercut:
        if (data._type == 'TLN' || data._type == 'TLB') {
          break
        } else {
          (function (id, userId, vehicleId, fields) {
            checkPowerCut(id, userId, vehicleId, fields)
          })(actions[i].id, actions[i].userId, actions[i].vehicleId, fields)
        }
        break
      case __alertHarshDrive:
        if (data._type == 'TLN' || data._type == 'TLB') {
          break
        } else {
          (function (userId, vehicleId, alertBy, fields) {
            checkHarshDrive(userId, vehicleId, alertBy, fields)
          })(alert.userId, alert.vehicleId, alert.alert, fields)
        }
        break
    }
  }
}

var calculateOverSpeed = function (id, userId, vehicleId, maxSpeed, maxSpeedAchieved, alertBy, fields) {
  if (maxSpeedAchieved) {
    if (fields.speed <= maxSpeed) {
      Alert.update({id: id}, {$set: {maxSpeedAchieved: false}}, function (err, updatedAlert) {
        if (err) {
          winston.log('error', err)
        }
        return
      })
    }
  // else --> still in high speed.
  } else {
    if (fields.n_speed > maxSpeed) {
      Alert.update({id: id}, {$set: {maxSpeedAchieved: true}}, function (err, updatedAlert) {
        if (err) {
          winston.log('error', err)
          return
        }

        Vehicle.findOne({id: vehicleId}, function (err, vehicle) {
          if (err) {
            winston.log('error', err)
            return
          }

          var message = 'Vehicle ' + vehicle.regNum + ' has found to be exceeding the speed limit of ' + maxSpeed.toString() + ' by running at ' + fields.n_speed.toString()

          sendAlert(userId, id, message, __alertOverSpeed, alertBy[__alertOverSpeed])
          return
        })
      })
    }

  // else --> Everything is cool!!
  }
}

var checkHarshDrive = function (userId, vehicleId, alertBy, fields) {
  if (!fields.a_ed)
    return
  else

    Vehicle.findOne({id: vehicleId}, function (err, vehicle) {
      if (err) {
        winston.log('error', err)
        return
      }

      if (!vehicle) {
        winston.log('info', 'Obsolate vehicle data present in database.')
        return
      }

      switch (a_ed) {
        case 1:
          var message = 'Harsh acceleration reported for ' + vehicle.regNum + ' at ' + fields.n_rtime
          break
        case 2:
          var message = 'Harsh braking reported for ' + vehicle.regNum + ' at ' + fields.n_rtime
          break
        case 3:
          var message = 'Harsh cornering reported for ' + vehicle.regNum + ' at ' + fields.n_rtime
          break
        default:
          return
      }

      sendAlert(userId, alertId, message, __alertHarshDrive, alertBy[__alertHarshDrive])
    })
}

/* 
 * Saving notification in table
 */
var saveNotification = function (userId, alertId, message, msgCode) {
  var notification = new Notification({userId: userId, alertId: alertId, message: message, msgCode: msgCode})
  notification.save(function (err) {
    if (err) {
      winston.log('error', err)
    }

    return
  })
}

var sendAlert = function (userId, alertId, message, msgCode, flag) {
  flag = parseInt(flag)
  if (flag == 0)
    return

  var binary = (flag >>> 0).toString(2)

  if (parseInt(binary[0]))
    saveNotification(userId, alertId, message, msgCode)
  if (parseInt(binary[1])) {
    // Email
  }
  if (parseInt(binary[2])) {
    // sms
  }
}

var checkPowerCut = function (id, userId, vehicleId, fields) {
  if (fields.a_epsr) {
    var message = 'The External Power Source of your device of vehicle ' + vehicleId + ' has been removed :)'

  // send alert
  }
}
