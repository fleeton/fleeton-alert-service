module.exports = {
  Alert: {
    userId: {
      type: String,
      required: true
    },
    vahicleId: {
      type: String,
      required: true
    },
    action: {
      type: String
    },
    alert: {
      type: String
    },
    expiryTime: {
      type: Date
    },
    maxSpeed: {
      type: Number
    },
    maxSpeedAchieved: {
      type: Boolean
    },
    geoFenceIds: {
      type: Array,
      required: false
    },
    isActive: {
      type: Boolean
    },
    createdAt: {
      type: Date
    },
    updatedAt: {
      type: Date
    }
  },

  Tracker: {
    phone: {
      type: String,
      required: true
    },

    imei: {
      type: String,
      required: true
    },

    vehicleId: {
      type: String,
      required: true
    },
    createdAt: {
      type: Date
    },
    updatedAt: {
      type: Date
    }
  },

  Notification: {
    userId: {
      type: String,
      required: true
    },

    alertId: {
      type: String,
      required: true
    },

    message: {
      type: String,
      required: true
    },

    msgCode: {
      type: Number,
      required: true
    },

    seen: {
      type: Boolean,
      defaultsTo: false
    },
    createdAt: {
      type: Date
    },
    updatedAt: {
      type: Date
    }
  },

  VehicleData: {
    imei: {
      type: String,
      required: true
    },
    _type: {
      // desc: packet type
      type: 'String'
    },
    n_rtime: {
      // desc: rtime
      type: Date,
      unique: true,
      required: true,
      index: true
    },
    n_loc: {
      // desc: loc
      type: [Number],
      required: true
    },
    n_speed: {
      // desc: speed
      type: Number,
      required: true
    },
    n_cog: {
      // desc: courseOverGround
      type: Number
    },
    n_alt: {
      // desc: altitude
      type: Number
    },
    n_nos: {
      // desc: noSatellite
      type: Number
    },
    n_gps: {
      // desc: gpsStatus
      type: Number
    },
    n_gss: {
      // desc: gsmSignalstrength
      type: Number
    },
    n_dis: {
      // desc: digitalInputStatus
      type: Number
    },
    n_dos: {
      // desc: digitalOutputStatus
      type: Number
    },
    n_aiv: {
      // desc: analogInputValue
      type: Number
    },
    n_eb: {
      // desc: ExternalBattery
      type: Number
    },
    n_ib: {
      // desc: internalBattery
      type: Number
    },
    n_ptem: {
      // desc: pcbTemperature
      type: Number
    },
    n_owd: {
      // desc: oneWireData
      type: Number
    },
    n_mv: {
      // desc: MovingStatus
      type: Number
    },
    n_agv: {
      // desc: accelGValue
      type: Number
    },
    n_tv: {
      // desc: tiltValue
      type: Number
    },
    n_trip: {
      // desc: trip
      type: Number
    },
    n_vo: {
      // desc: virtualOdometer
      type: Number
    },
    n_psn: {
      // desc: packetSerialNumber
      type: Number
    },
    n_chksm: {
      // desc: checksum
      type: Number
    },

    // The TLA has all this, the 'A' is alert!
    a_ig: {
      // desc: individualGeofenceAlert
      type: Number
    },
    a_rf: {
      // desc: rfid
      type: String
    },
    a_os: {
      // desc: overSpeedAlert
      type: Number
    },
    a_i2m: {
      // desc: input2MisuseAlert
      type: Number
    },
    a_im: {
      // desc: immobilizerAlert
      type: Number
    },
    a_etemp: {
      // desc: extTemperatureAlert
      type: Number
    },
    a_ed: {
      // desc: ecoDrivingAlert
      type: Number
    },
    a_dr: {
      // desc: deviceReconnectAlert
      type: Number
    },
    a_ibl: {
      // desc: internalBatteryLowAlert
      type: Number
    },
    a_epsr: {
      // desc: extPowerSrcRemovedAlert
      type: Number
    },
    a_gmf: {
      // desc: gpsModuleFailureAlert
      type: Number
    },
    a_tw: {
      // desc: towAwayAlert
      type: Number
    },
    a_snr: {
      // desc: serverNotReachable
      type: Number
    },
    a_smode: {
      // desc: sleepMode
      type: Number
    },
    a_sc: {
      // desc: smsCount
      type: Number
    }
  },

  Vehicle: {
    vehicleType: {
      type: Number,
      required: true
    },

    regNum: {
      type: String,
      required: true
    },

    addedBy: {
      type: String,
      required: true
    }
  }
}
